/*
* @Author: asus
* @Date:   2017-12-10 10:44:02
* @Last Modified by:   asus
* @Last Modified time: 2017-12-17 19:49:52
*/
 const path            = require('path');
 var webpack           = require('webpack');
 var ExtractTextPlugin = require("extract-text-webpack-plugin");
 var HtmlWebpackPlugin = require('html-webpack-plugin');
 var WEBPACK_ENV       = process.env.WEBPACK_ENV || 'dev';
//获取html-webpack-plugin参数方法
 var getHtmlConfig     = function(name){
    return {
                template  : './src/view/' + name +'.html',
                filename  : 'view/' + name +'.html',
                inject    : true,
                hash      : true,
                chunks    : ['common',name]
            };
 } 
 var config = {
     entry: {
        'common' : ['./src/page/common/index.js'],
     	'index' :  ['./src/page/index/index.js'],
     	'login' :  ['./src/page/login/login.js'],
     },
     output: {
         path:path.resolve(__dirname, 'dist'),
         publicPath : '/dist',
         filename: 'js/[name].js'
     },
     externals:{
     	 'jquery' : 'window.jQuery'
     },
      module: {
        rules:[
              { 
                test: /\.css$/, 
                use : ExtractTextPlugin.extract({
                    use: 'css-loader',
                    fallback: 'style-loader'
                }) 
              },
              { test: /\.(gif|png|jpg|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=100&name=resource/[name].[ext]' },
        ]
     },
     resolve : {
        alias : {
            node_modules    : __dirname + '/node_modules',
            util            : __dirname + '/src/util',
            page            : __dirname + '/src/page',
            service         : __dirname + '/src/service',
            image           : __dirname + '/src/image',
        }
     },
     plugins:[
                //独立通用模块js/base.js
         		new webpack.optimize.CommonsChunkPlugin({
         			name: 'common', 
         			filename:'js/base.js'
         		}),
                //css单独打包插件
                new ExtractTextPlugin("css/[name].css"),
                //html模板处理
                new HtmlWebpackPlugin(getHtmlConfig('index')),
                new HtmlWebpackPlugin(getHtmlConfig('login')),
            ]
  };

  if('dev' === WEBPACK_ENV){
    config.entry.common.push('webpack-dev-server/client?http://localhost:8090/')
  }

 module.exports = config;