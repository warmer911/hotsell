/*
* @Author: asus
* @Date:   2017-12-23 11:58:11
* @Last Modified by:   asus
* @Last Modified time: 2017-12-23 12:06:09
*/

var _mm = require('util/mm.js');
var _cart = {
	//获取购物数量
	getCartCount : function(resolve,reject){
		_mm.request({
			url    : _mm.getServerUrl('/cart/get_cart_product_count.do'),
			success: resolve,
			error  : reject
		});
	},
	
}

module.exports = _cart;