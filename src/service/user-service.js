/*
* @Author: asus
* @Date:   2017-12-23 11:35:29
* @Last Modified by:   asus
* @Last Modified time: 2017-12-23 11:53:19
*/
var _mm = require('util/mm.js');
var _user = {
	//检查登录状 态
	checkLogin : function(resolve,reject){
		_mm.request({
			url    : _mm.getServerUrl('/user/get_user_info.do'),
			method : 'POST',
			success: resolve,
			error  : reject
		});
	},
	//登出
	logout : function(resolve,reject){
		_mm.request({
			url    : _mm.getServerUrl('/user/logout.do'),
			method : 'POST',
			success: resolve,
			error  : reject
		});
	},
}

module.exports = _user;